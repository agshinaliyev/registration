package problems.dto;

import lombok.Data;

@Data
public class UserResetPasswordDto {

    private String email;
    private String emailVerificationCode;
    private String newPassword;
    private String confirmNewPassword;

}
