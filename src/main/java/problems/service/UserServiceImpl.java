package problems.service;

import problems.dto.UserLoginDto;
import problems.dto.UserRegisterDto;
import problems.dto.UserResetPasswordDto;
import problems.model.User;
import org.springframework.stereotype.Service;

@Service
public interface UserServiceImpl {

    void userRegister(UserRegisterDto userRegisterDto);
    String userLogin(UserLoginDto userLoginDto);
    void resetPassword(UserResetPasswordDto userResetPasswordDto);
    User findUserByUsername(String username);
}
