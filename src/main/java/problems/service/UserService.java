package problems.service;

import problems.dto.UserLoginDto;
import problems.dto.UserRegisterDto;
import problems.dto.UserResetPasswordDto;
import lombok.RequiredArgsConstructor;
import problems.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import problems.repository.UserRepository;

@Service
@RequiredArgsConstructor

public class UserService implements UserServiceImpl {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    @Override
    public void userRegister(UserRegisterDto userRegisterDto) {


        User user = modelMapper.map(userRegisterDto, User.class);
        userRepository.save(user);


    }
    @Override
    public String userLogin(UserLoginDto userLoginDto) {

            User byUsername = userRepository.getByUsername(userLoginDto.getUsername());
            if ((byUsername.getUsername().equals(userLoginDto.getUsername()) &&
                    (byUsername.getPassword().equals(userLoginDto.getPassword()))))
                return "You're logged in successfully";

            return "Access denied.Check username or password and try again!";


    }


        @Override
        public void resetPassword(UserResetPasswordDto userResetPasswordDto) {
            User user = userRepository.getByEmail(userResetPasswordDto.getEmail());
            user.setPassword(userResetPasswordDto.getNewPassword());
            userRepository.save(user);
        }


    @Override
    public User findUserByUsername(String username) {

        return userRepository.getByUsername(username);
    }





}

